# ########################################################### #
# Pyxel detector simulation framework                         #
#                                                             #
# Example yaml configuration file                             #
# CCD Photon Transfer Curve with parametric analysis mode     #
# Created by D. Lucsanyi (ESA)                                #
# ########################################################### #



parametric:

  mode: sequential
  parameters:
    - key: pipeline.photon_generation.illumination.arguments.level
      values: numpy.unique(numpy.logspace(0, 6, 10, dtype=int))
    - key: detector.characteristics.qe
      values: [0.1,0.2,0.3,0.4,0.5,0.6]
    - key: pipeline.readout_electronics.ac_crosstalk.arguments.coupling_matrix
      values: [[[1, 0.5, 0, 0], [0.5, 1, 0, 0], [0, 0, 1, 0.5], [0, 0, 0.5, 1]], [[1, 0.3, 0, 0], [0.3, 1, 0, 0], [0, 0, 1, 0.3], [0, 0, 0.3, 1]]]

  outputs:
    output_folder:  'outputs'
    save_data_to_file:
      - detector.image.array:   ['npy']
    save_parametric_data:
      - dataset:   ['nc']
      - parameters:   ['nc']
      - logs:   ['nc']

ccd_detector:

  geometry:

    row:   100                          # pixel
    col:   100                          # pixel
    total_thickness: 10.                # um
    pixel_vert_size: 10.                # um
    pixel_horz_size: 10.                # um

  material:
    material: 'silicon'

  environment:

  characteristics:
    qe: 0.5                 # -
    eta: 1.                 # e/photon
    sv: 1.e-6               # V/e
    amp: 0.8                # V/V
    a1: 100.                # V/V
    a2: 65536               # DN/V
    fwc: 100000             # e
    fwc_serial: 200000      # e

pipeline:

  # -> photon
  photon_generation:
    - name: illumination
      func: pyxel.models.photon_generation.illumination
      enabled: true
      arguments:
          level: 0
    - name: shot_noise
      func: pyxel.models.photon_generation.shot_noise
      enabled: true

  # photon -> photon
  optics:

  # photon -> charge
  charge_generation:
    - name: photoelectrons
      func: pyxel.models.charge_generation.simple_conversion
      enabled: true

  # charge -> pixel
  charge_collection:
    - name: simple_collection
      func: pyxel.models.charge_collection.simple_collection
      enabled: true
    - name: fixed_pattern_noise
      func: pyxel.models.charge_collection.fix_pattern_noise
      enabled: true
      arguments:
        pixel_non_uniformity: data/pixel_non_uniformity.npy
    - name: full_well
      func: pyxel.models.charge_collection.simple_full_well
      enabled: true

  # pixel -> pixel
  charge_transfer:

  # pixel -> signal
  charge_measurement:
    - name: simple_measurement
      func: pyxel.models.charge_measurement.simple_measurement
      enabled: true
    - name: output_node_noise
      func: pyxel.models.charge_measurement.output_node_noise
      enabled: true
      arguments:
        std_deviation: 1.e-6   # Volt

  # signal -> image
  readout_electronics:
    - name: simple_amplifier
      func: pyxel.models.readout_electronics.simple_amplifier
      enabled: true
    - name: ac_crosstalk
      func: pyxel.models.readout_electronics.ac_crosstalk
      enabled: true
      arguments:
        coupling_matrix: [[1, 0.5, 0, 0], [0.5, 1, 0, 0], [0, 0, 1, 0.5], [0, 0, 0.5, 1]]
        channel_matrix: [1,2,3,4]
        readout_directions: [1,2,1,2]
    - name: simple_processing
      func: pyxel.models.readout_electronics.simple_processing
      enabled: true



