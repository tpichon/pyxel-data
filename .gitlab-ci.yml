variables:
  # Change pip's cache directory to be inside the project directory.
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

  TWINE_USERNAME: gitlab-ci-token
  TWINE_PASSWORD: $CI_JOB_TOKEN

stages:
  - build
  - examples
  - tutorials
  - use_cases

# Use for temporary storage for project dependencies(e.g. pypi packages...)
cache:
  paths:
    - $CI_PROJECT_DIR/.cache/pip

before_script:
  - pip install -U pip
  - pip install -U wheel setuptools tox

build_wheel:
  image: python:3.8
  stage: build
  script:
    - pip install twine

    # Get source code
    - git clone https://$GIT_USER:$GIT_PASSWORD@gitlab.com/esa/pyxel.git

    # Create wheel (.whl) and source archive (.tar.gz) file.
    - cd pyxel
    - python setup.py sdist bdist_wheel

    # - twine upload --repository-url https:
    # - twine upload --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*

    # Send the wheel file to folder ~/dist
    - mv dist/ ..

  artifacts:
    paths:
      - dist/
  rules:
    # If `$FORCE_GITLAB_CI` is set, create a pipeline.
    - if: '$FORCE_GITLAB_CI'

single_master:
  image: python:${PYTHON}
  stage: examples
  needs:
    - build_wheel
  script:
    # Install Pyxel
    - pip install `ls dist/*.whl`[all]

    - tox -e examples-${PIPELINE}-${MODE}
  parallel:
    matrix:
      - PYTHON: ["3.7", "3.8"]
        PIPELINE: [single]
        MODE: [cli, notebook]
  rules:
    - if: '$FORCE_GITLAB_CI'

single_pypi:
  image: python:${PYTHON}
  stage: examples
  needs: []
  script:
    # Install Pyxel
    - pip install pyxel-sim[all]

    - tox -e examples-${PIPELINE}-${MODE}
  parallel:
    matrix:
      - PYTHON: ["3.7", "3.8"]
        PIPELINE: [single]
        MODE: [cli, notebook]
  rules:
    - if: '$FORCE_GITLAB_CI'

parametric_master:
  image: python:${PYTHON}
  stage: examples
  needs:
    - build_wheel
  script:
    # Install Pyxel
    - pip install `ls dist/*.whl`[all]

    - tox -e examples-${PIPELINE}-${MODE}
  parallel:
    matrix:
      - PYTHON: ["3.7", "3.8"]
        PIPELINE: [parametric]
        MODE: [cli_product, cli_sequential, cli_custom,notebook_product, notebook_sequential, notebook_custom]
  rules:
    - if: '$FORCE_GITLAB_CI'

parametric_pypi:
  image: python:${PYTHON}
  needs: []
  stage: examples
  script:
    # Install Pyxel
    - pip install pyxel-sim[all]

    - tox -e examples-${PIPELINE}-${MODE}
  parallel:
    matrix:
      - PYTHON: ["3.7", "3.8"]
        PIPELINE: [parametric]
        MODE: [cli_product, cli_sequential, cli_custom,notebook_product, notebook_sequential, notebook_custom]
  rules:
    - if: '$FORCE_GITLAB_CI'

dynamic_master:
  image: python:${PYTHON}
  stage: examples
  needs:
    - build_wheel
  script:
    # Install Pyxel
    - pip install `ls dist/*.whl`[all]

    - tox -e examples-${PIPELINE}-${MODE}
  parallel:
    matrix:
      - PYTHON: ["3.7", "3.8"]
        PIPELINE: [dynamic_persistence]
        MODE: [cli, notebook]
  rules:
    - if: '$FORCE_GITLAB_CI'

dynamic_pypi:
  image: python:${PYTHON}
  needs: []
  stage: examples
  script:
    # Install Pyxel
    - pip install pyxel-sim[all]

    - tox -e examples-${PIPELINE}-${MODE}
  parallel:
    matrix:
      - PYTHON: ["3.7", "3.8"]
        PIPELINE: [dynamic_persistence]
        MODE: [cli, notebook]
  rules:
    - if: '$FORCE_GITLAB_CI'

calibration_master:
  image: python:${PYTHON}
  stage: examples
  needs:
    - build_wheel
  script:
    # Install Pyxel
    - pip install `ls dist/*.whl`[all]

    - tox -e examples-${PIPELINE}-${MODE}
  parallel:
    matrix:
      - PYTHON: ["3.7", "3.8"]
        PIPELINE: [calibration]
        MODE: [cli, notebook]
  rules:
    - if: '$FORCE_GITLAB_CI'

calibration_pypi:
  image: python:${PYTHON}
  needs: []
  stage: examples
  script:
    # Install Pyxel
    - pip install pyxel-sim[all]

    - tox -e examples-${PIPELINE}-${MODE}
  parallel:
    matrix:
      - PYTHON: ["3.7", "3.8"]
        PIPELINE: [calibration]
        MODE: [cli, notebook]
  rules:
    - if: '$FORCE_GITLAB_CI'

models_master:
  image: python:${PYTHON}
  stage: examples
  needs:
    - build_wheel
  script:
    # Install Pyxel
    - pip install `ls dist/*.whl`[all]

    - tox -e examples-${PIPELINE}-${MODE}
  parallel:
    matrix:
      - PYTHON: ["3.7", "3.8"]
        PIPELINE: [model_amplifier, model_inter_pixel]
        MODE: [cli, notebook]
  rules:
    - if: '$FORCE_GITLAB_CI'

models_pypi:
  image: python:${PYTHON}
  needs: []
  stage: examples
  script:
    # Install Pyxel
    - pip install pyxel-sim[all]

    - tox -e examples-${PIPELINE}-${MODE}
  parallel:
    matrix:
      - PYTHON: ["3.7", "3.8"]
        PIPELINE: [model_amplifier, model_inter_pixel]
        MODE: [cli, notebook]
  rules:
    - if: '$FORCE_GITLAB_CI'

tutorials_master:
  image: python:${PYTHON}
  stage: tutorials
  needs:
    - build_wheel
  script:
    # Install Pyxel
    - pip install `ls dist/*.whl`[all]

    - tox -e tutorials-${TUTORIAL}
  parallel:
    matrix:
      - PYTHON: ["3.7", "3.8"]
        TUTORIAL: [0, 1, 2, 4, 5, 6, 7]
  rules:
    - if: '$FORCE_GITLAB_CI'

tutorials_pypi:
  image: python:${PYTHON}
  needs: []
  stage: tutorials
  script:
    # Install Pyxel
    - pip install pyxel-sim[all]

    - tox -e tutorials-${TUTORIAL}
  parallel:
    matrix:
      - PYTHON: ["3.7", "3.8"]
        TUTORIAL: [0, 1, 2, 4, 5, 6, 7]
  rules:
    - if: '$FORCE_GITLAB_CI'

# use_cases_py38:
#   image: python:3.8
#   stage: use_cases
#   needs:
#     - build_wheel
#   script:
#     # Install Pyxel
#     - pip install `ls dist/*.whl`[all]

#     - tox -e use_cases-${PIPELINE}-${MODE}
#   parallel:
#     matrix:
#       - PIPELINE: [flex]
#         MODE: [cli, notebook]
#   rules:
#     - if: '$FORCE_GITLAB_CI'
